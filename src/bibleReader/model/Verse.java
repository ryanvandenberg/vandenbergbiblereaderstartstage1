package bibleReader.model;

/**
 * A class which stores a verse.
 * 
 * @author Charles Cusack (Skeleton Code), 2008, edited 2012.
 * @author ? (provided the implementation)
 */
public class Verse implements Comparable<Verse> {
	
	// TODO Add Fields here. (There should be 2 of them--one for the reference and one for the text of the verse.)

	/**
	 * Construct a verse given the reference and the text.
	 * 
	 * @param r The reference for the verse
	 * @param t The text of the verse
	 */
	public Verse(Reference r, String t) {
		// TODO Implement me: Stage 2
	}

	/**
	 * Construct a verse given the book, chapter, verse, and text.
	 * 
	 * @param book The book of the Bible
	 * @param chapter The chapter number
	 * @param verse The verse number
	 * @param text The text of the verse
	 */
	public Verse(BookOfBible book, int chapter, int verse, String text) {
		// TODO Implement me: Stage 2
	}

	/**
	 * Returns the Reference object for this Verse.
	 * 
	 * @return A reference to the Reference for this Verse.
	 */
	public Reference getReference() {
		// TODO Implement me: Stage 2
		return null;
	}

	/**
	 * Returns the text of this Verse.
	 * 
	 * @return A String representation of the text of the verse.
	 */
	public String getText() {
		// TODO Implement me: Stage 2
		return "";
	}

	/**
	 * Returns a String representation of this Verse, which is a String representation of the Reference followed by the
	 * String representation of the text of the verse.
	 */
	@Override
	public String toString() {
		// TODO Implement me: Stage 2
		return "";
	}

	/**
	 * Should return true if and only if they have the same reference and text.
	 */
	@Override
	public boolean equals(Object other) {
		// TODO Implement me: Stage 2
		// Don't forget the rules for how equals and hashCode are supposed to be related.
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Implement me: Stage 2
		// Don't forget the rules for how equals and hashCode are supposed to be related.
		return -1;
	}

	/**
	 * From the Comparable interface. See the API for how this is supposed to work.
	 */
	@Override
	public int compareTo(Verse other) {
		// TODO Implement me: Stage 2
		return 0;
	}

	/**
	 * Return true if and only if this verse the other verse have the same reference. (So the text is ignored).
	 * 
	 * @param other the other Verse.
	 * @return true if and only if this verse and the other have the same reference.
	 */
	public boolean sameReference(Verse other) {
		return getReference().equals(other.getReference());
	}

}
